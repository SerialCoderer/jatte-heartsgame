Jatte
=============== #

A client/server network based gaming server that uses TCP protocol for communication between server and clients. Multiple, distinct games will run concurrently on a single server. The server will manage lifecycle of these games. The server provides the capability for players to play against each other in a single game.

The games are network based using Java Sockets for network communication. The persistence layer consist of mysql.
In order to play a game a player must have a user account that consist of username, password, win/loss record history.

Games will be deployed on the following platforms ios devices, android devices, Facebook, desktop computers, embedded devices such as raspberry pi, and accessible from web browsers.

How to run Jatte Server from IDE

Jatte Server
In Intellij run Jatte-Server
A dailog will come up this is where you can change the port the server will run on. Default is 9001.
Click "Starting" button to run server
Jatte Client - Hearts Client
In Intellij run Heart-Client.
login with credentials in the populateJatte.sql for access credentials
In order to start the hearts multi-player network game you will need to create/start 4 clients
![Hearts Game](heartsSnippet.png)
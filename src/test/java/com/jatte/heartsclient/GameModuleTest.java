package com.jatte.heartsclient;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.jatte.GameHost;
import com.jatte.GamePort;
import com.jatte.heartsclient.login.HeartsGameSessionTest;



/**
 * Created by jovaughnlockridge1 on 11/20/16.
 */
public class GameModuleTest extends AbstractModule {


    @Override
    protected void configure() {
        bind(HeartsGameSessionTest.class).in(Singleton.class);
        bindConstant().annotatedWith(GameHost.class).to("localhost");
        bindConstant().annotatedWith(GamePort.class).to(new Integer(9001));
    }
}

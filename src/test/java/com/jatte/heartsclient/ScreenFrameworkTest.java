package com.jatte.heartsclient;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by jovaughnlockridge1 on 11/20/16.
 */
public class ScreenFrameworkTest extends Application{

    private Stage primaryStage;
    public static final String LOGIN_SCREEN = "views/login";

    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
        this.primaryStage = primaryStage;
        ScreensControllerTest mainContainer = new ScreensControllerTest();


        mainContainer.loadScreen(ScreensFramework.HEARTS_GAME_SCREEN,
                ScreensFramework.HEARTS_GAME_SCREEN_FXML);

        mainContainer.setScreen(ScreensFramework.HEARTS_GAME_SCREEN);

        Group root = new Group();
        root.getChildren().addAll(mainContainer);

        Scene scene  = new Scene(root, 1198, 1132);
        scene.getStylesheets().add(getClass().getResource("/cards.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {


            launch(args);

    }


    @Override
    public void stop(){
        this.primaryStage.close();
        System.exit(0);
    }
}

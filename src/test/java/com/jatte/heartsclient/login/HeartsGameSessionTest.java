package com.jatte.heartsclient.login;

import com.google.inject.Singleton;
import com.jatte.heartsgame.HeartsGame;
import com.jatte.heartsgame.States.Dealing;
import com.jatte.service.GameStateObservable;
import com.jatte.service.ServerManager;


import static org.mockito.Mockito.mock;

/**
 * Created by jovaughnlockridge1 on 11/20/16.
 */
@Singleton
public class HeartsGameSessionTest {

    private HeartsGame heartsGame = new HeartsGame();

    public HeartsGameSessionTest(){
        ServerManager sm = mock(ServerManager.class);
        heartsGame.setGameStateObservable(new GameStateObservable(sm));
        heartsGame.addPlayer("tester1");
        heartsGame.addPlayer("tester2");
        heartsGame.addPlayer("tester3");
        heartsGame.addPlayer("tester4");
        heartsGame.startGame();
        heartsGame.setGameState(new Dealing(heartsGame));
    }
}

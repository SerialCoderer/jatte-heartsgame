package com.jatte.heartsclient.login;

import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;
import com.jatte.client.Move;
import com.jatte.heartsclient.HeartsSubject;
import com.jatte.heartsclient.gameview.GameViewModel;
import com.jatte.heartsgame.HeartsGame;
import com.jatte.heartsgame.HeartsGamePlayer;
import com.jatte.service.IllegalMoveException;
import com.jatte.service.serverRequest.GameNetworkRequest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;


import static com.jatte.heartsclient.model.GameAction.UPDATE_SCREEN;
import static com.jatte.heartsgame.HeartsGameMoveType.PASS_LEFT;
import static org.mockito.Mockito.mock;

/**
 * Created by jlockrid on 10/10/2016.
 */
public class GameViewModelTest {

    private HeartsSubject heartsSubject = new HeartsSubject(0, "");
    private GameViewModel gameViewModel = new GameViewModel(heartsSubject);
    private HeartsGame heartsGame = new HeartsGame();
    private HeartsGamePlayer tester1;


    @Before
    public void setup() throws Exception {
        heartsGame.addPlayer("tester1");
        heartsGame.addPlayer("tester2");
        heartsGame.addPlayer("tester3");
        heartsGame.addPlayer("tester4");
        heartsGame.startGame();


        tester1 = heartsGame.getPlayer("tester1");

    }


    @Test
    public void Should_removeOneCardFromHand_When_OneSuccessfulMoveIsSent() {


        List<Card<Suits, Integer>> cards = tester1.getPlayerCards();
        Move move = new Move(PASS_LEFT, Arrays.asList(cards.get(0)));
        gameViewModel.addCardToMove(cards.get(0), true);
        gameViewModel.sendMove();


    }


    @Test
    public void testingUI(){

    }
}

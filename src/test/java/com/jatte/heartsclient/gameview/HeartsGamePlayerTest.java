package com.jatte.heartsclient.gameview;

import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;
import com.jatte.heartsgame.HeartsGamePlayer;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jovaughnlockridge1 on 9/17/16.
 */
public class HeartsGamePlayerTest {

    HeartsGamePlayer player1 = new HeartsGamePlayer("jova");
    Card<Suits, Integer> twoOfClubs = new Card(Suits.CLUBS, new Integer(2), "2");

    @Before
    public void setUp() throws Exception {



    }


    @Test
    public void shouldRemoveCollectionOfCardsFromPlayer() throws Exception {

        List<Card<Suits, Integer>> cards = new ArrayList<>();
        cards.add(new Card(Suits.CLUBS, new Integer(2), "2"));
        cards.add(new Card(Suits.CLUBS, new Integer(3), "3"));
        cards.add(new Card(Suits.CLUBS, new Integer(4), "4"));
        player1.addPlayerCards(cards);
        Assert.assertEquals(3, player1.getPlayerCards().size());
        player1.removePlayerCards(cards);
        Assert.assertEquals(0, player1.getPlayerCards().size());

    }
}

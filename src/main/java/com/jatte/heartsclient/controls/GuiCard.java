package com.jatte.heartsclient.controls;

import com.jatte.api.cards.Card;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.Skin;

/**
 * Created by jovaughnlockridge1 on 10/8/16.
 */
public class GuiCard extends Label {

    private CardObservableValue cardObservableValue = new CardObservableValue();


    private boolean clicked = false;

    public GuiCard(){
        super();
        setPrefSize(73.0, 97.0);
        getStyleClass().add("guiCard");
    }
    public void setCard(Card card){
        cardObservableValue().setValue(card);
    }

    public CardObservableValue cardObservableValue() { return cardObservableValue; }

    public Card getCard() {
        return cardObservableValue.get();
    }

    @Override
    protected Skin<?> createDefaultSkin(){
        return new GuiCardSkin(this);
    }


    private ObjectProperty<EventHandler<ActionEvent>> onAction =
            new ObjectPropertyBase<EventHandler<ActionEvent>>() {

                @Override
                protected void invalidated() {
                    setEventHandler(ActionEvent.ACTION, get());
                }

                @Override
                public Object getBean() {
                    return GuiCard.this;
                }

                @Override
                public String getName() {
                    return "onAction";
                }
            };


    public final void setOnAction(EventHandler<ActionEvent> value){
        onActionProperty().set(value);
    }

    public final ObjectProperty<EventHandler<ActionEvent>> onActionProperty() {
        return onAction;
    }

    public boolean isClicked() {
        return clicked;
    }

    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }


    @Override
    public boolean equals(Object o){

       if(o==this){
           return true;
       }

        if(o instanceof GuiCard){
            return ((GuiCard) o).getCard().equals(this.getCard());
        }

        return false;
    }


}

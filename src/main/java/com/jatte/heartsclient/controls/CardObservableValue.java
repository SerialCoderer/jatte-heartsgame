package com.jatte.heartsclient.controls;


import com.jatte.api.cards.Card;
import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by jovaughnlockridge1 on 9/6/16.
 */
public class CardObservableValue extends SimpleObjectProperty<Card> {

    private Card card;
    private List<InvalidationListener> list = new ArrayList<>();

    @Override
    public void removeListener(ChangeListener<? super Card> listener) {
        System.out.println("ChangeListener remove ");
    }

    @Override
    public Card getValue() {
        return get();
    }

    @Override
    public void addListener(InvalidationListener listener) {
        list.add(listener);
    }

    @Override
    public void removeListener(InvalidationListener listener) {
        list = list.
                stream().
                filter(l -> l.equals(listener)).
                collect(Collectors.toList());
    }

    public void setValue(Card card){
        this.card = card;
        set(card);
        list.stream().forEach( l -> l.invalidated(this));
    }

    @Override
    public Card get() {
        return card;
    }
}

package com.jatte.heartsclient.controls;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jovaughnlockridge1 on 10/9/16.
 */
public class CardPane extends Pane {

    private SimpleObjectProperty<GuiCard> addCardProperty = new SimpleObjectProperty<>();
    private ChangeListener<GuiCard> addCardUpdate = (x, y, z) -> addCard(x,y,z);

    public CardPane() {
        super();
        setPrefHeight(400);
        setPrefWidth(400);
        addCardProperty().addListener(addCardUpdate);

    }



    public SimpleObjectProperty<GuiCard> addCardProperty (){
        return addCardProperty;
    }




    public void addCard(ObservableValue o, GuiCard oldValue, GuiCard newValue){
        System.out.println("old "+oldValue+" new "+newValue+" o "+o);
        getChildren().add(newValue);
    }

    @Override
    protected void layoutChildren() {
        List<Node> sortedChildren = new ArrayList<>(getChildren());

        double currentX = 0.0;
        for(Node c : sortedChildren) {
            layoutInArea(c, currentX+50, 0.0, 500, 200, 0, HPos.CENTER, VPos.BASELINE );
            currentX+=50;
        }



    }
}

package com.jatte.heartsclient.controls;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jovaughnlockridge1 on 10/9/16.
 */
public class PlayingVerticalHandPane extends Pane {

    private SimpleObjectProperty<GuiCard> addCardProperty = new SimpleObjectProperty<>();
    private ChangeListener<GuiCard> addCardUpdate = (x, y, z) -> addCard(x,y,z);

    private SimpleObjectProperty<GuiCard> removeCardProperty = new SimpleObjectProperty<>();
    private ChangeListener<GuiCard> removeCardUpdate = (x, y, z) -> addCard(x,y,z);

    public PlayingVerticalHandPane() {
        super();
        addCardProperty().addListener(addCardUpdate);
    }


    public SimpleObjectProperty<GuiCard> addCardProperty (){
        return addCardProperty;
    }

    public SimpleObjectProperty<GuiCard> removeCardProperty (){
        return removeCardProperty;
    }


    public void addCard(ObservableValue o, GuiCard oldValue, GuiCard newValue){

        getChildren().add(newValue);
    }

    @Override
    protected void layoutChildren() {
        List<Node> sortedChildren = new ArrayList<>(getChildren());
        double currentY = 0.0;

        for(Node c : sortedChildren) {
            c.setRotate(90);
            layoutInArea(c, 0.0 , currentY, 200, 800, 0, HPos.CENTER, VPos.BASELINE );
            currentY+=40;
        }
    }
}

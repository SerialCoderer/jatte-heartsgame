package com.jatte.heartsclient.login;

import com.google.inject.Inject;
import com.jatte.heartsclient.IScreensController;
import com.jatte.heartsclient.HeartsSubject;
import com.jatte.heartsclient.ControlledScreen;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * Created by jovaughnlockridge1 on 8/1/16.
 */
public class LoginView implements ControlledScreen {

    @FXML
    private TextField userNameTextField;

    @FXML
    private TextField passwordTextField;

    @FXML
    private Button loginButton;

    @FXML
    private Text actiontarget;

    @FXML
    private Button connectButton;

    private IScreensController controller;

    private LoginViewModel loginViewModel;

    @Inject
    private HeartsSubject heartsClient;

    @FXML
    void initialize() {
        loginViewModel = new LoginViewModel(heartsClient);
        userNameTextField.textProperty().bindBidirectional(loginViewModel.userNameProperty());
        passwordTextField.textProperty().bindBidirectional(loginViewModel.passwordProperty());
        loginButton.setOnAction(v -> loginViewModel.login());
        actiontarget.textProperty().bind(loginViewModel.isLoggedIn());
        loginButton.disableProperty().bind(loginViewModel.disableLoginButton());
        connectButton.disableProperty().bind(loginViewModel.disableLoginButton().not());
        loginButton.disableProperty().bind(loginViewModel.isLoginPossibleProperty().not());
        connectButton.setOnAction(v -> loginViewModel.connect());
    }

    @Override
    public void setScreenParent(IScreensController screenPage) {
        this.controller = screenPage;
        loginViewModel.setScreenParent(screenPage);
    }
}

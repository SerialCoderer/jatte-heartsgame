package com.jatte.heartsclient.login;


import com.jatte.heartsclient.HeartsSubject;
import com.jatte.heartsclient.ControlledScreen;
import com.jatte.heartsclient.IScreensController;
import com.jatte.heartsclient.ScreensFramework;
import com.jatte.service.serverRequest.ServerActions;
import javafx.beans.property.*;

/**
 * Created by jovaughnlockridge1 on 7/30/16.
 */
public class LoginViewModel implements ControlledScreen {

    private final StringProperty userName = new SimpleStringProperty();
    private final StringProperty password = new SimpleStringProperty();
    private final ReadOnlyBooleanWrapper loginPossible = new ReadOnlyBooleanWrapper();
    private final ReadOnlyBooleanWrapper loggedIn = new ReadOnlyBooleanWrapper();
    private final String label = "Welcome";
    private final ReadOnlyStringWrapper labelText = new ReadOnlyStringWrapper(label);
    private boolean isLoggedIn;
    private final HeartsSubject client;


    private IScreensController controller;


    public LoginViewModel(HeartsSubject client){
        this.client = client;
        loginPossible.bind(userName.isNotEmpty().and(password.isNotEmpty()));
    }

    public StringProperty userNameProperty() {
        return userName;
    }
    public StringProperty passwordProperty() {
        return password;
    }
    public ReadOnlyBooleanProperty isLoginPossibleProperty() {
        return loginPossible.getReadOnlyProperty();
    }

    public SimpleStringProperty isLoggedIn() {
        return labelText;
    }

    public BooleanProperty disableLoginButton(){
        return loggedIn;
    }

    public void login(){
        isLoggedIn = client.login(userName.get(), password.getValue());
        loggedIn.set(true);
        if(isLoggedIn){
            labelText.set("Logged In");
        }else{
            labelText.set("Not Logged In");
        }
    }

    public void connect(){
        client.gameUpdates(ServerActions.JOIN_GAME,"Hearts");
        controller.setScreen(ScreensFramework.HEARTS_GAME_SCREEN);
    }

//    @Override
//    public void updateGameScreen(GameUpdateEvent event) {
//        System.out.println("Screen event "+event);
//    }

    @Override
    public void setScreenParent(IScreensController screenPage) {
        this.controller = screenPage;
    }

}

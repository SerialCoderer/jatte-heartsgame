package com.jatte.heartsclient;

import com.google.inject.Inject;

import com.jatte.GameHost;
import com.jatte.GamePort;
import com.jatte.PlayerMove;
import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;
import com.jatte.client.Move;
import com.jatte.heartsgame.HeartsGamePlayer;
import com.jatte.proto.GameServiceGrpc;
import com.jatte.proto.Jatte;
import com.jatte.service.serverRequest.ServerActions;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by jovaughnlockridge1 on 7/31/16.
 */

public class HeartsSubject {

    private static final Logger logger = LoggerFactory.getLogger(HeartsSubject.class.getName());
    private final GameServiceGrpc.GameServiceBlockingStub blockingStub;
    private final GameServiceGrpc.GameServiceStub asyncStub;
    private String networkId;
    private final List<HeartGameObserver> heartGameListeners = new ArrayList<>();
    private HeartsGameUpdate gameStateUpdate;
    private ServerActions gameAction;
    private String userName;

    @Inject
    public HeartsSubject(@GamePort int port, @GameHost String host) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port)
                // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
                // needing certificates.
                .usePlaintext()
                .build();
        blockingStub = GameServiceGrpc.newBlockingStub(channel);
        asyncStub = GameServiceGrpc.newStub(channel);
    }

    public boolean login(String name, String password) {
        Jatte.LoginRequest request = Jatte
                .LoginRequest
                .newBuilder()
                .setUsername(name)
                .setPassword(password)
                .build();
        Jatte.LoginReply response = null;
        try {
            response = blockingStub.login(request);
        } catch (StatusRuntimeException e) {
            logger.error("RPC failed: {}", e.getStatus());
        }
        userName = name;
        networkId = response.getNetworkId();
        logger.info("{} with network id {}, {}", response.getMessage(), networkId);
        return response.getIsLoggedIn();
    }

    public void move(List<Card> move){
        gameUpdates(ServerActions.MOVE, "Hearts", move);
    }

    public void gameUpdates(ServerActions serverAction, String gameName) {
        StreamObserver<Jatte.GameUpdateRequest> requestOberver = asyncStub
                .gameUpdate(new StreamObserver<Jatte.GameUpdateReply>() {
                    @Override
                    public void onNext(Jatte.GameUpdateReply value) {
                        gameStateUpdate = transformGameUpdateReply(value);
                        notifyAllObservers(ServerActions.valueOf(value.getServerAction()));
                        logger.info("Got value {}", value);
                    }

                    @Override
                    public void onError(Throwable t) {
                        logger.info("Error  {}", t);
                    }

                    @Override
                    public void onCompleted() {
                        logger.info("Completed  {}");
                    }
                });
        Jatte.GameUpdateRequest gameUpdateRequest = Jatte.GameUpdateRequest
                .newBuilder()
                .setUuid(this.networkId)
                .setServerAction(serverAction.name())
                .setGameName(gameName)
                .setMoveRequest(Jatte.MoveRequest.newBuilder().build())
                .build();
        requestOberver.onNext(gameUpdateRequest);

    }

    public void gameUpdates(ServerActions serverAction, String gameName, List<Card> move) {
        StreamObserver<Jatte.GameUpdateRequest> requestOberver = asyncStub
                .gameUpdate(new StreamObserver<Jatte.GameUpdateReply>() {
                    @Override
                    public void onNext(Jatte.GameUpdateReply value) {
                        gameStateUpdate = transformGameUpdateReply(value);
                        notifyAllObservers(ServerActions.valueOf(value.getServerAction()));
                        logger.info("Got value {}", value);
                    }

                    @Override
                    public void onError(Throwable t) {
                        logger.info("Error  {}", t);
                    }

                    @Override
                    public void onCompleted() {
                        logger.info("Completed  {}");
                    }
                });
        Jatte.MoveRequest moveRequest;
        if(move != null) {
           moveRequest = Jatte.MoveRequest.newBuilder().build();
        }else {
            moveRequest = Jatte.MoveRequest
                    .newBuilder()
                    .addAllMove(move.stream().map(c -> transformCard(c)).collect(Collectors.toList()))
                    .build();
        }
        Jatte.GameUpdateRequest gameUpdateRequest = Jatte.GameUpdateRequest
                .newBuilder()
                .setUuid(this.networkId)
                .setServerAction(serverAction.name())
                .setGameName(gameName)
                .setMoveRequest(moveRequest)
                .build();
        requestOberver.onNext(gameUpdateRequest);
    }

    private void notifyAllObservers(ServerActions gameAction) {
        heartGameListeners.stream().forEach(l -> l.gameUpdate(gameAction, gameStateUpdate));
    }

    public void registerListener(HeartGameObserver o) {
        heartGameListeners.add(o);
    }

    public HeartsGameUpdate transformGameUpdateReply(Jatte.GameUpdateReply gameUpdateReply) {
        List<HeartsGamePlayer> heartsGamePlayers = new ArrayList<>();
        for (Jatte.Player p : gameUpdateReply.getPlayersList()) {
            HeartsGamePlayer player = new HeartsGamePlayer(p.getUsername());
            List<Card<Suits, Integer>> heartsPlayerCards = new ArrayList<>();
            for(Jatte.Card jatteCard : p.getCardsList() ) {
                Card card = new Card<>(Suits.valueOf(jatteCard.getSuit()), jatteCard.getValue(), jatteCard.getFaceCard());
                heartsPlayerCards.add(card);
            }
            player.addPlayerCards(heartsPlayerCards);
            player.addPlayerScore(p.getPlayerScore());
            heartsGamePlayers.add(player);
        }
        HeartsGameUpdate gameStateUpdate = new HeartsGameUpdate(gameUpdateReply.getGameAction(), heartsGamePlayers);
        return gameStateUpdate;
    }

    public String getUserName(){
        return this.userName;
    }

    private Jatte.Card transformCard(Card card) {
        return Jatte.Card.newBuilder()
                .setFaceCard(card.getFace())
                .setSuit(card.getSuit().toString())
                .setValue(card.getCardValue().intValue())
                .build();
    }
}

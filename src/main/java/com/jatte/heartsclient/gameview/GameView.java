package com.jatte.heartsclient.gameview;

import com.google.inject.Inject;
import com.jatte.api.cards.Card;
import com.jatte.heartsclient.IScreensController;
import com.jatte.heartsclient.ScreensFramework;
import com.jatte.heartsclient.controls.*;
import com.jatte.heartsclient.ControlledScreen;
import com.jatte.heartsclient.HeartsSubject;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by jovaughnlockridge1 on 8/13/16.
 */
public class GameView implements ControlledScreen, Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(GameView.class);

    @Inject
    private GameViewModel gameViewModel;

    private IScreensController controller;

    @FXML
    private Button move;

    @FXML
    private Label name;

    @FXML
    private Label statusBar;

    @FXML
    private PlayingHandPane playerOne;

    @FXML
    private PlayingVerticalHandPane playerTwo;

    @FXML
    private PlayingVerticalHandPane playerFour;

    @FXML
    private PlayingHandPane playerThree;

    @FXML
    private CardPane playedTable;

    @FXML
    private DeckOfCardsPane deckOfCards;

    @FXML
    private BorderPane pane;

    @Inject
    private HeartsSubject heartsClient;

    @FXML
    public void initialize(URL url, ResourceBundle resourceBundle) {
        gameViewModel.deal().addListener((x, y, z) -> deal(x, y, z));
        gameViewModel.getCardBinder().addListener((x, y, z) -> createNewCardControl(x, y, z));
        gameViewModel.getGuiPlayingHandCardRemover().addListener((x, y, z) -> removeGuiCardFromHand(x, y, z));
        gameViewModel.getPlayingTableCardBinder().addListener((x, y, z) -> addCardToPlayingTable(x, y, z));
        gameViewModel.getRemovedPlayingTableCardBinder().addListener((x, y, z) -> removeCardFromPlayingTable(x, y, z));
        move.setOnAction(t -> gameViewModel.sendMove());
        gameViewModel.getStatusBar().addListener((x, y, z) -> updateStatusBar(x, y, z));
        name.textProperty().bindBidirectional(gameViewModel.getPlayerName());
        deckOfCards.init();
    }

    public void updateStatusBar(Observable o, String oldText, String newText) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                statusBar.setText(newText);
            }
        });
    }

    public void deal(Observable o, List<Card> old, List<Card> newCards) {

        LOGGER.info("adding these cards to screen {} ", newCards);
        if (newCards != null) {
            for (Card newCard : newCards) {
                GuiCard g = new GuiCard();
                g.getStyleClass().add("card");
                g.setCard(newCard);
                g.setOnAction(e -> {
                    if (!g.isClicked()) {
                        g.setTranslateY(g.getLayoutY() - 25);
                        g.setClicked(true);
                    } else {
                        g.setTranslateY(g.getLayoutY() + 25);
                        g.setClicked(false);
                    }
                   gameViewModel.addCardToMove(g.getCard(), g.isClicked());
                });
                LOGGER.info("Adding card {} to screen ", newCard);

                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        deckOfCards.dealToPlayerOne();
                    }
                });

                try {
                    Thread.sleep(200);
                } catch (InterruptedException ie) {
                }

                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        playerOne.getChildren().add(g);
                    }
                });
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        deckOfCards.dealToPlayerTwo();
                    }
                });

                try {
                    Thread.sleep(200);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }

                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        GuiCard card = new GuiCard();
                        card.setCard(new Card(null, null, new String("bluecard"), null));
                        playerTwo.getChildren().add(card);
                    }
                });

                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        deckOfCards.dealToPlayerThree();
                    }
                });

                try {
                    Thread.sleep(200);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }


                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        GuiCard card = new GuiCard();
                        card.setCard(new Card(null, null, new String("bluecard"), null));
                        playerThree.getChildren().add(card);
                    }
                });

                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        deckOfCards.dealToPlayerFour();
                    }
                });

                try {
                    Thread.sleep(200);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }

                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        GuiCard card = new GuiCard();
                        card.setCard(new Card(null, null, new String("bluecard"), null));
                        playerFour.getChildren().add(card);
                    }
                });
            }
            LOGGER.info("Done Adding cards {} to screen ");
        }


    }

    public void createNewCardControl(Observable o, List<Card> old, List<Card> newCards) {

        LOGGER.info("adding these cards to screen {} ", newCards);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {

                LOGGER.info("adding these cards to screen {} ", newCards);
                if (newCards != null) {
                    newCards.stream().forEach(
                            newCard ->
                            {
                                GuiCard g = new GuiCard();
                                g.getStyleClass().add("card");
                                g.setCard(newCard);
                                g.setOnAction(e -> {
                                    if (!g.isClicked()) {
                                        g.setTranslateY(g.getLayoutY() - 25);
                                        g.setClicked(true);
                                    } else {
                                        g.setTranslateY(g.getLayoutY() + 25);
                                        g.setClicked(false);
                                    }
                                    gameViewModel.addCardToMove(g.getCard(), g.isClicked());
                                });
                                LOGGER.info("Adding card {} to screen ", newCard);
                                playerOne.getChildren().add(g);
                            }
                    );
                }
            }
        });

    }

    public void removeGuiCardFromHand(Observable o, List<Card> old, List<Card> newCard) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (newCard != null) {
                    newCard.stream().forEach(
                            e -> {
                                LOGGER.info("Removing card {} from screen ", e);
                                GuiCard c = new GuiCard();
                                c.setCard(e);
                                playerOne.getChildren().remove(c);
                            }
                    );
                } else {
                    playerOne.getChildren().clear();
                }
            }
        });
    }


    public void addCardToPlayingTable(Observable o, List<Card> old, List<Card> newCard) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {

                if (newCard != null) {
                    newCard.stream().forEach(e -> {
                        GuiCard g = new GuiCard();
                        g.setCard(e);
                        playedTable.getChildren().add(g);
                    });
                }
            }
        });

    }


    public void removeCardFromPlayingTable(Observable o, List<Card> old, List<Card> newCard) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (newCard != null) {
                    newCard.stream().forEach(
                            e -> {
                                GuiCard c = new GuiCard();
                                c.setCard(e);
                                playedTable.getChildren().remove(c);
                            }
                    );
                }
            }
        });
    }


    @Override
    public void setScreenParent(IScreensController screenPage) {
        this.controller = screenPage;
    }


    @FXML
    private void goToMain() {
        controller.setScreen(ScreensFramework.LOGIN_SCREEN);
    }

}


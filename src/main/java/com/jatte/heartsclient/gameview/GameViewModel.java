package com.jatte.heartsclient.gameview;

import com.google.inject.Inject;
import com.jatte.PlayerMove;
import com.jatte.api.cards.Card;


import com.jatte.api.cards.Suits;
import com.jatte.client.Move;
import com.jatte.heartsclient.HeartGameObserver;
import com.jatte.heartsclient.HeartsGameUpdate;
import com.jatte.heartsclient.HeartsSubject;
import com.jatte.heartsgame.HeartsGameMoveType;
import com.jatte.heartsgame.HeartsGamePlayer;
import com.jatte.service.serverRequest.ServerActions;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by jovaughnlockridge1 on 8/12/16.
 */


public class GameViewModel implements HeartGameObserver {

    private static final Logger LOGGER = LoggerFactory.getLogger(GameViewModel.class);
    private SimpleObjectProperty<List<Card>> cardBinder = new SimpleObjectProperty<>();
    private SimpleObjectProperty<List<Card>> deal = new SimpleObjectProperty<>();
    private SimpleObjectProperty<List<Card>> guiPlayingHandCardRemover = new SimpleObjectProperty<>();
    private SimpleObjectProperty<List<Card>> playingTableCardBinder = new SimpleObjectProperty<>();
    private SimpleObjectProperty<List<Card>> removedPlayingTableCardBinder = new SimpleObjectProperty<>();

    private StringProperty playerName = new SimpleStringProperty();
    private StringProperty status = new SimpleStringProperty();
    private Lock screenLock = new ReentrantLock();
    private HeartsGameMoveType moveType;


    private String userName;
    private List<Card> playingHand = new ArrayList<>();
    private List<Card> table = new ArrayList<>(4);
    private List<Card> listCards = new ArrayList<>();
    private String currentGameState = null;
    private final HeartsSubject heartsSubject;
    private Comparator<Card<Suits, Integer>> cardComparator = (o1, o2) -> {
            if (o1.getSuit() == o2.getSuit()) {
                if (o1.getOrdinal() > o2.getOrdinal()) {
                    return 1;
                } else {
                    return -1;
                }
            } else if (o1.getSuit() == Suits.HEARTS && o2.getSuit() != Suits.HEARTS) {
                return 1;
            } else if (o1.getSuit() != Suits.HEARTS && o2.getSuit() == Suits.HEARTS) {
                return -1;
            } else if (o1.getSuit() == Suits.CLUBS &&
                    (o2.getSuit() == Suits.DIAMONDS ||
                            o2.getSuit() == Suits.SPADES)) {
                return 1;
            } else if ((o1.getSuit() == Suits.DIAMONDS || o1.getSuit() == Suits.SPADES) && o2.getSuit() == Suits.CLUBS) {
                return -1;
            } else if (o1.getSuit() == Suits.SPADES && o2.getSuit() == Suits.DIAMONDS) {
                return 1;
            } else if (o1.getSuit() != Suits.DIAMONDS && o2.getSuit() == Suits.SPADES) {
                return -1;
            }
            return -1;
        };

    @Inject
    public GameViewModel(HeartsSubject heartsClient) {
        this.heartsSubject = heartsClient;
        heartsClient.registerListener((gameAction, heartsGame) -> gameUpdate(gameAction, heartsGame));

    }

    @Override
    public void gameUpdate(ServerActions serverAction, HeartsGameUpdate gameStateUpdate) {
        LOGGER.info("Received server action {}", serverAction);
        switch (serverAction) {
            case JOIN_GAME_SUCCESSFUL:
                LOGGER.info("JOIN GAME SUCCESSFUL");
                break;
            case LOGIN_SUCCESSFUL:
                LOGGER.info("Logging successful for username {}", this.userName);
                break;

            case MESSAGE:
//                this.status.setValue((String) event.getData());
                break;

            case START:
                LOGGER.info("Got the start message");
            case MOVE:
                this.currentGameState = gameStateUpdate.getGameAction();
                LOGGER.info("Got game state is {} ", this.currentGameState);
                screenLock.lock();
                try {
                    Map<String, HeartsGamePlayer> players = gameStateUpdate.getGamePlayers()
                            .stream()
                            .collect(Collectors.toMap(HeartsGamePlayer::getUserName, Function.identity()));
                    clearPlayingTable();
                    clearHand();
                    List<Card<Suits, Integer>> cardsPlayed = new ArrayList<>();
                    List<Card<Suits, Integer>> playerHand = players.get(heartsSubject.getUserName()).getPlayerCards();
                    Collections.sort(playerHand, cardComparator);
                    table.addAll(cardsPlayed);
                    playingHand.addAll(playerHand);
                    LOGGER.info("Cards left in hand {} ", playingHand);
                    dealCards(playingHand);
                    playingTableCardBinder.set(null);
                    playingTableCardBinder.set(table);

                } finally {
                    screenLock.unlock();
                }
                break;
            case UPDATE_SCREEN:
                screenLock.lock();
                this.currentGameState = gameStateUpdate.getGameAction();
                LOGGER.info("Got Screen Update {} ", this.currentGameState);
                screenLock.lock();
                try {
                    Map<String, HeartsGamePlayer> players = gameStateUpdate.getGamePlayers()
                            .stream()
                            .collect(Collectors.toMap(HeartsGamePlayer::getUserName, Function.identity()));
                    clearPlayingTable();
                    clearHand();
                    List<Card<Suits, Integer>> cardsPlayed = new ArrayList<>();
                    List<Card<Suits, Integer>> playerHand = players.get(heartsSubject.getUserName()).getPlayerCards();
                    Collections.sort(playerHand, cardComparator);
                    table.addAll(cardsPlayed);
                    playingHand.addAll(playerHand);
                    playingTableCardBinder.set(null);
                    playingTableCardBinder.set(table);

                } finally {
                    screenLock.unlock();
                }
                break;
        }
    }


    public SimpleObjectProperty<List<Card>> getCardBinder() {
        return cardBinder;
    }

    public SimpleObjectProperty<List<Card>> deal() {
        return deal;
    }


    public SimpleObjectProperty<List<Card>> getGuiPlayingHandCardRemover() {
        return guiPlayingHandCardRemover;
    }

    public StringProperty getStatusBar() {
        return status;
    }

    public StringProperty getPlayerName() {
        return playerName;
    }


    /**
     * This is used to create a game move. You will either
     * add card a to your move or remove the card from your
     * move.
     *
     * @param card       the card added to your move.
     * @param addingCard determines if the card is added to the move or removed if present.
     */
    public void addCardToMove(Card card, boolean addingCard) {
        screenLock.lock();
        try {
            if (addingCard) {
                playingHand.remove(card);
                listCards.add(card);
                LOGGER.info("Attempting to send card {} ", card);
            } else {
                playingHand.add(card);
                listCards.remove(card);
                LOGGER.info("Removing attempt to send card {} ", card);
            }
        } finally {
            screenLock.unlock();
        }
    }

    public void sendMove() {
        if (listCards == null || listCards.isEmpty()) {
            return;
        }
        screenLock.lock();
        List<Card> copyOfCardsMove = null;
        try {
            copyOfCardsMove = listCards.stream().collect(Collectors.toList());
            listCards.clear();
            removeCardFromPlayerHandOnGui(copyOfCardsMove);
        } finally {
            screenLock.unlock();
        }
        LOGGER.info("Sending cards={}", copyOfCardsMove);
        move(copyOfCardsMove);
    }

    private void move(List<Card> move) {
        screenLock.lock();
        try {
            heartsSubject.move(move);
        } finally {
            screenLock.unlock();
        }
    }

    private void clearHand() {
        screenLock.lock();
        try {
            List<Card> copyOfCardsInHand = playingHand.stream().collect(Collectors.toList());
            removeCardFromPlayerHandOnGui(null);
            removeCardFromPlayerHandOnGui(copyOfCardsInHand);
            playingHand.clear();
        } finally {
            screenLock.unlock();
        }
    }

    private void removeCardFromPlayerHandOnGui(List<Card> cards) {
        guiPlayingHandCardRemover.set(cards);
    }

    private void addCardToScreen(List<Card> cards) {
        cardBinder.set(null);
        cardBinder.set(cards);
    }

    private void dealCards(List<Card> cards) {
        deal.set(null);
        deal.set(cards);
    }

    private void clearPlayingTable() {
        removedPlayingTableCardBinder.set(null);
        if (!table.isEmpty()) {
            List<Card> copyOfCardsOnTable = table.stream().collect(Collectors.toList());
            removedPlayingTableCardBinder.set(copyOfCardsOnTable);
            table.clear();
        }
    }


    public SimpleObjectProperty<List<Card>> getRemovedPlayingTableCardBinder() {
        return removedPlayingTableCardBinder;
    }

    public SimpleObjectProperty<List<Card>> getPlayingTableCardBinder() {
        return playingTableCardBinder;
    }


}

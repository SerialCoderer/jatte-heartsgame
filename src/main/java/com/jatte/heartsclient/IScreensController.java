package com.jatte.heartsclient;

import java.io.IOException;

/**
 * Created by jovaughnlockridge1 on 11/20/16.
 */
public interface IScreensController {

    void loadScreen(String id, String resource) throws IOException;
    boolean setScreen(final String name);

}

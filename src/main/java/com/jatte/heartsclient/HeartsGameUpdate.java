package com.jatte.heartsclient;

import com.jatte.heartsgame.HeartsGamePlayer;

import java.util.Collection;

public class HeartsGameUpdate {
    private String gameAction;
    private Collection<HeartsGamePlayer> gamePlayers;

    public HeartsGameUpdate(String gameAction, Collection<HeartsGamePlayer> gamePlayers) {
        this.gameAction = gameAction;
        this.gamePlayers = gamePlayers;
    }

    public String getGameAction() {
        return gameAction;
    }

    public Collection<HeartsGamePlayer> getGamePlayers() {
        return gamePlayers;
    }
}

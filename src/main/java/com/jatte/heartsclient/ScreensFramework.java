package com.jatte.heartsclient;/**
 * Created by jovaughnlockridge1 on 7/30/16.
 */

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ScreensFramework extends Application {

    private Scene scene;
    private Stage primaryStage;
    public static final String LOGIN_SCREEN = "views/login";
    public static final String LOGIN_SCREEN_FXML = "/views/login/loginView.fxml";
    public static final String HEARTS_GAME_SCREEN = "hearts";
    public static final String HEARTS_GAME_SCREEN_FXML = "/views/login/heartsView.fxml";

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        ScreensController mainContainer = new ScreensController();
        mainContainer.loadScreen(ScreensFramework.LOGIN_SCREEN, ScreensFramework.LOGIN_SCREEN_FXML);
        mainContainer.loadScreen(ScreensFramework.HEARTS_GAME_SCREEN, ScreensFramework.HEARTS_GAME_SCREEN_FXML);
        mainContainer.setScreen(LOGIN_SCREEN);
        Group root = new Group();
        root.getChildren().addAll(mainContainer);
        scene = new Scene(root, 1198, 1132);
        scene.getStylesheets().add(getClass().getResource("/cards.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void stop() {
        primaryStage.close();
    }
}

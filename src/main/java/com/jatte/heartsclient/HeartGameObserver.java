package com.jatte.heartsclient;

import com.jatte.service.serverRequest.ServerActions;

public interface HeartGameObserver {

    void gameUpdate(ServerActions gameAction, HeartsGameUpdate gameUpdate);
}

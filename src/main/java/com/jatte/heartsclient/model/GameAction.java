package com.jatte.heartsclient.model;

/**
 * Created by jovaughnlockridge1 on 8/15/16.
 */
public enum GameAction {

    START_GAME, MESSAGE, UPDATE_SCREEN
}

package com.jatte.heartsclient;

import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by jovaughnlockridge1 on 8/13/16.
 */
public class ScreensController extends StackPane implements IScreensController {
    private static final Logger logger = LoggerFactory.getLogger(ScreensController.class);

    private HashMap<String, Node> screens = new HashMap<>();
    private Injector injector;

    public void loadScreen(String id, String resource) throws IOException {
        if (injector == null) {
            injector = Guice.createInjector(new GameModule());
        }
        FXMLLoader myLoader = new FXMLLoader(getClass().getResource(resource));
        myLoader.setControllerFactory(i -> {
            return injector.getInstance(i);
        });
        Parent loadScreen = myLoader.load();
        ControlledScreen screen = myLoader.getController();
        screen.setScreenParent(this);
        addScreen(id, loadScreen);
    }

    private void addScreen(String name, Node screen) {
        screens.put(name, screen);
    }

    public boolean setScreen(final String name) {

        if (screens.get(name) != null) { //screen loaded
            final DoubleProperty opacity = opacityProperty();

            //If there is more than one screen
            if (!getChildren().isEmpty()) {
                Timeline fade = new Timeline(
                        new KeyFrame(Duration.ZERO,
                                new KeyValue(opacity, 1.0)),
                        new KeyFrame(new Duration(1000),

                                t -> {
                                    //remove displayed screen
                                    getChildren().remove(0);
                                    //add new screen
                                    getChildren().add(0, screens.get(name));
                                    Timeline fadeIn = new Timeline(
                                            new KeyFrame(Duration.ZERO,
                                                    new KeyValue(opacity, 0.0)),
                                            new KeyFrame(new Duration(800),
                                                    new KeyValue(opacity, 1.0)));
                                    fadeIn.play();
                                }, new KeyValue(opacity, 0.0)));
                fade.play();
            } else {
                //no one else been displayed, then just show
                setOpacity(0.0);
                getChildren().add(screens.get(name));
                Timeline fadeIn = new Timeline(
                        new KeyFrame(Duration.ZERO,
                                new KeyValue(opacity, 0.0)),
                        new KeyFrame(new Duration(2500),
                                new KeyValue(opacity, 1.0)));
                fadeIn.play();
            }
            return true;
        } else {

            logger.warn("Could not load screen {} ", name);
            return false;

        }
    }

    public boolean unloadScreen(String name) {
        if (screens.remove(name) == null) {
            System.out.println("Screen didn't exist");
            return false;
        } else {
            return true;
        }
    }

}

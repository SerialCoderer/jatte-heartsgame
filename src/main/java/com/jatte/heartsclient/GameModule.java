package com.jatte.heartsclient;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.jatte.GameHost;
import com.jatte.GamePort;


/**
 * Created by jovaughnlockridge1 on 8/31/16.
 */
public class GameModule  extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(GameHost.class).to("localhost");
        bindConstant().annotatedWith(GamePort.class).to(Integer.valueOf(9001));
        bind(HeartsSubject.class).in(Singleton.class);
    }
}

